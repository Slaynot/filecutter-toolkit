# Filecutter Toolkit

A collection of tools to help the filecutting process for the Skywind project.

See the [Wiki](https://gitlab.com/Slaynot/filecutter-toolkit/-/wikis/home) for more informations.

Credits:

- Krisscracka (Audacity macro)
- Slaynot (LibreOffice python script)
- bradyjam (Linux Install Script)